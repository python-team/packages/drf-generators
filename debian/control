Source: drf-generators
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/brobin/drf-generators
Vcs-Browser: https://salsa.debian.org/python-team/packages/drf-generators
Vcs-Git: https://salsa.debian.org/python-team/packages/drf-generators.git

Package: python3-djangorestframework-generators
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: python3-djangorestframework
Description: Generate DRF Serializers, Views, and URLs (Python3 version)
 Writing APIs can be boring and repetitive work. With DRF Generators, one simple
 command will generate all Views, Serializers, and even Urls for a Django Rest
 Framework application.
 .
 This is not intended to give a production quality API. It was intended to
 jumpstart development and save developers from writing the same code over and
 over for each model.
 .
 This package contains the Python 3 version of the library.
